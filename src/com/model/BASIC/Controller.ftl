package com.fps.web.controller.${table_name?lower_case};
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.fps.web.model.${table_name};
import com.fps.web.service.${table_name}Service;
import com.fpswork.core.model.ExcelEntity;
import com.fpswork.core.model.PageEntity;
import com.fpswork.util.StringUtil;
import com.fps.web.controller.BaseController;

/**
 * ${table_annotation}页面
 *
 */
@Controller
@RequestMapping("/${table_name?lower_case}")
public class ${table_name}Controller extends BaseController{
    
    @Autowired
    ${table_name}Service ${table_name?uncap_first}Service;
    
    
 
   
    /**
	 * 加载List数据
	 */
    @RequestMapping(value = "/load${table_name}List", method = RequestMethod.GET)
    public @ResponseBody PageEntity load${table_name}List(PageEntity pageEntity) throws Exception{
    	${table_name?uncap_first}Service.query${table_name}List(pageEntity);
        return pageEntity;
    }
    
    
    
    /**
   	 * ${table_annotation}管理
   	 */
    @RequestMapping("/${table_name?lower_case}Management")
    public ModelAndView ${table_name?lower_case}Management(String id,String pagetype) throws Exception{
    	ModelAndView modelAndView = new ModelAndView();   
    	${table_name} ${table_name?lower_case}= new ${table_name}();
    	if(!"add".equals(pagetype)){
    		${table_name?lower_case} = ${table_name?uncap_first}Service.query${table_name}ByID(id);
    	}
    	modelAndView.addObject("${table_name?lower_case}",${table_name?lower_case});
    	modelAndView.addObject("pagetype", pagetype);
		modelAndView.setViewName("web/${table_name?lower_case}/${table_name?lower_case}management");      
        return modelAndView;
    }
    
    /**
   	 * ${table_annotation}管理新增表单
   	 */
    @RequestMapping(value = "/${table_name?lower_case}AddSubmit", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> ${table_name?lower_case}AddSubmit(@ModelAttribute("${table_name}")${table_name} ${table_name?uncap_first}) throws Exception{
    	${table_name?uncap_first}Service.add${table_name}(${table_name?uncap_first});
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("flag", "true");
        return map;
    }
     
    /**
   	 * ${table_annotation}管理修改表单
   	 */
    @RequestMapping(value = "/${table_name?lower_case}UpdateSubmit", method = RequestMethod.POST)
    public @ResponseBody ${table_name} ${table_name?lower_case}UpdateSubmit(@ModelAttribute("${table_name}")${table_name} ${table_name?uncap_first}) throws Exception{
    	${table_name?uncap_first}Service.update${table_name}(${table_name?uncap_first});
        return ${table_name?uncap_first};
    }
    
    
    /**
   	 * ${table_annotation}删除
   	 */
    @RequestMapping(value = "/delete${table_name}", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> delete${table_name}(@ModelAttribute("${table_name}")${table_name} ${table_name?uncap_first}) throws Exception{
    	${table_name?uncap_first}Service.delete${table_name}(${table_name?uncap_first});
    	Map<String, String> map = new HashMap<String, String>();
		map.put("flag", "true");
        return map;
    }
    
	/**
   	 * DownExcel
   	 * 下载${table_annotation}excel
   	 */
    @RequestMapping(value = "/DownExecl${table_name}", method = RequestMethod.GET)
    public @ResponseBody Map<String, String> DownExecl${table_name}(HttpServletResponse res,HttpServletRequest req,String exceldata) throws Exception{
       Map<String, String> map = new HashMap<String, String>();
       	ExcelEntity excelEntity = new ExcelEntity();
       	//存放对应的表头名称和对应的数据库字段
    	Map<String, Object> titlemap = new LinkedHashMap<String, Object>();
    	//下载文件名
    	String fileName=".xls";
       	if(exceldata.contains("||") ){// 以"||"分割的字符串
	    	  List<String> arg=  StringUtil.splitString(exceldata, "||");
	    	  for(int i=0;i<arg.size();i++){
	    		  if(i==0){
	    			  fileName=arg.get(0)+fileName; 
	    		  }else{
	    			  String[] arg1=   arg.get(i).split(":"); 
	    			  titlemap.put(arg1[0], arg1[1]);
	    		  }
	    	  }
	    }
       
    	excelEntity.setFilename(fileName);
    	excelEntity.setLinename(titlemap);
    	//直接查询结果集的语句
    	String sql="select * from ${table_name} where 1=1";
    	//eg:
		//if(StringUtil.isNotEmpty(menuname)){
		//	sql+=" and basemenu.menuname='"+menuname+"' ";
		//}
    	excelEntity.setSql(sql);
    	this.DownExecl(res, req, excelEntity);
    	map.put("flag", "true");
        return map;
    }
}
