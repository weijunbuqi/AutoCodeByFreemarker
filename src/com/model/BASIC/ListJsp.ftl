<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <title>${table_annotation}页面</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<!--  1、 框架标签引入区 （start）-->
<!--  1、 框架标签引入区 （end）-->
<!--  2、 CSS声明区 （start）-->
<!--  2.0、引入CSS区 （start）-->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/other/layui/css/layui.css"/>
  	<link rel="stylesheet" href="<%=request.getContextPath() %>/freeStyle/css/saber.css"/>
<!--  2.0、 引入CSS区 （end）-->
<!--  2.1、本界面CSS区 （start）-->
<!--  2.1、 本界面CSS区 （end）-->
<!--  2、 CSS声明区 （end）-->
<!--  3、JS声明区 （start）-->
<!--  3.0、 导入外部JS区 （start）-->
<!--  3.0、 导入外部JS区 （end）-->
<!--  3.1、 全局变量声明区 （start）-->
<script type="text/javascript">

</script>
<!--  3.1、 全局变量声明区 （end）--> 
<!--  3.2、 初始化区 （start）-->
<script type="text/javascript">

</script>
<!--  3.2、 初始化区 （end）-->
<!--  3.3、 页面控制区 （start）-->
<script type="text/javascript">

</script>
<!--  3.3、 页面控制区 （end）-->
<!--  3.4、 事件区 （start）-->
<script type="text/javascript">
function add${table_name}(id,type){
	var url ="<%=request.getContextPath() %>/${table_name?lower_case}/${table_name?lower_case}Management.html?id="+id+"&pagetype="+type+"";
	layer.open({
		  type: 2 //此处以iframe举例
	        ,title: ['新增','font-size:18px;background-color:#fff;color:#000']
	        ,area: [$(document.body).width()-50+'px', $(document.body).height()-50+'px']
	        ,shade: 0
	        ,maxmin: true
	        ,content: url
	        ,zIndex: layer.zIndex //重点1
	        ,end :function(){
	        	layui.table.reload('${table_name?lower_case}List', {
            		  url: '<%=request.getContextPath() %>/${table_name?lower_case}/load${table_name}List.html'
            		  ,where: {} //设定异步数据接口的额外参数
            	});
	        }
		});    
}
function DownExecl${table_name}(){
	var exceldata ="${table_annotation}数据"
	<#if model_column?exists>
        <#list model_column as model>
       	exceldata+="||${model.changeColumnName?lower_case}:${model.columnComment!}"
	   </#list>
    </#if>
	var menuname ="" // $("#menuname").val();
	window.location.href='<%=request.getContextPath() %>/basemenu/DownExeclBasemenu.html?exceldata='+exceldata+'&menuname='+menuname;
}
function reloadList(){
	tableIns.reload({
		  where: { //设定异步数据接口的额外参数，任意设
			//menuname:$("#menuname").val()
		  }
		  ,page: {
		    curr: 1 //重新从第 1 页开始
		  }
		});
}
</script>
<!--  3.4、 事件区 （end）-->
<!--  3.5、 回调区 （start）-->
<script type="text/javascript">
		
</script>
<!--  3.5、 回调区 （end）-->
<!--  3.6、 业务逻辑区 （start）-->
<!--  3.6、 业务逻辑区 （end）-->

<!--  3、 JS声明区 （end）-->
<body class="body">
<input type="hidden" name="exceldata" id="exceldata" >
	<div class="toobar">
		<div class="btn-left">
		 	<button onclick="add${table_name}('','add')" class="layui-btn fps-btn-color layui-btn-radius layui-btn-sm"><i class="layui-icon">&#xe654;</i>${table_annotation}新增</button>
		</div>
		<div class="btn-right">
		 	<button onclick="DownExecl${table_name}()" class="layui-btn  layui-btn-radius fps-btn-color fps-btn-sm"><i class="layui-icon">&#xe601;</i>Excel导出</button>
		</div>
	</div>
	<div style="margin: 90px 50px 30px;background:#fff;padding:20px;">
		<div style="overflow:hidden;padding-bottom:10px;">
			<div style="float:left">
				<!-- <p style="float:left;line-height:32px;">菜单：</p>
				<input type="text" name="menuname" id="menuname" style="float:left;width:206px;height:32px;margin:0 2px;border:1px solid #ddd;"  value="" placeholder="请输入菜单"  class="layui-input">
			-->
			</div>
			<button class="layui-btn fps-btn-color fps-btn-sm" onclick="reloadList()"><i class="layui-icon">&#x1002;</i></button>
		</div>
		<table class="layui-hide" id="${table_name?lower_case}List" lay-filter="${table_name?lower_case}List"></table>
	</div>
</body>
 <script type="text/html" id="barTool">
  <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/other/layui/layui.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/dateformat.js"></script>
<script type="text/javascript">
var tableIns;
layui.use('table', function(){
	  var table = layui.table,form = layui.form;
	  tableIns = table.render({
	    elem: '#${table_name?lower_case}List'
	    ,url: '<%=request.getContextPath() %>/${table_name?lower_case}/load${table_name}List.html'
  		,where: {} //设定异步数据接口的额外参数
	    ,cols: [[ //标题栏
	       {type:'numbers'}
	      ,{type: 'checkbox'}
	      ,{field: 'id', title: 'ID'}
	<#if model_column?exists>
        <#list model_column as model>
       	<#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
  		,{field: '${model.changeColumnName?lower_case}', title: '${model.columnComment!}'}
    	</#if>
    	<#if model.columnType = 'DATETIME' >
    	,{field: '${model.changeColumnName?lower_case}', title: '${model.columnComment!}',templet:'<div>{{ Format(d.${model.changeColumnName?lower_case},"yyyy-MM-dd")}}</div>'  }
    	</#if>
	   </#list>
    </#if>
	      ,{fixed: 'right', width:178, align:'center', toolbar: '#barTool'}
	    ]]
	   // ,data: dateJson
	  	,page: true
	  });
	//监听工具条
	  table.on('tool(${table_name?lower_case}List)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'detail'){
	    	add${table_name}(data.id,"detail");
	    } else if(obj.event === 'del'){
	      layer.confirm('请确认是否删除？', function(index){
	    	  $.ajax({
	              url:"<%=request.getContextPath() %>/${table_name?lower_case}/delete${table_name}.html",
	              type:"post",
	              async: false,
	              data:{id:data.id},
	              success:function(res){
	      	        layer.close(index);
	      	      	layui.table.reload('${table_name?lower_case}List', {
            		  url: '<%=request.getContextPath() %>/${table_name?lower_case}/load${table_name}List.html'
            		  ,where: {} //设定异步数据接口的额外参数
            		});
	              },
	              error:function(e){
	            	  layer.alert("删除失败！");
	              }
	          });  
	      
	      });
	    } else if(obj.event === 'edit'){
	    	add${table_name}(data.id,"edit");
	    }
	  });
	});

</script>
</body>
</html>