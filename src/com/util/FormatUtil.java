package com.util;

public class FormatUtil {

	
	//格式化字符串，并去除字符串中的指定符号，首字母大写
	 public static String replaceUnderLineAndUpperCase(String str){
	        StringBuffer sb = new StringBuffer();
	        //将字符串全部小写
	        sb.append(str.toLowerCase());
	        //首字母大写
	        char ss = sb.charAt(0);
            char ia = (char) (ss - 32);
            sb.replace(0 , 0 + 1,ia + "");
            //将各个符号后的字母大写
	        int count = sb.indexOf("_");
	        while(count!=0){
	            int num = sb.indexOf("_",count);
	            count = num + 1;
	            if(num != -1){
	                 ss = sb.charAt(count);
	                 ia = (char) (ss - 32);
	                sb.replace(count , count + 1,ia + "");
	            }
	        }
	        //去掉符号
	        String result = sb.toString().replaceAll("_","");
	        return result;
	    }
	
}
