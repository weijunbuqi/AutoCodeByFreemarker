package com.fps.web.dao.impl;
import org.springframework.stereotype.Repository;
import com.fpswork.core.exception.exception.BusinessException;
import com.fpswork.core.model.PageEntity;
import com.fpswork.datamodel.basedao.BaseDao;
import com.fpswork.util.BeanUtils;
import com.fpswork.util.StringUtil;
import com.fps.web.dao.${table_name}Dao;
import com.fps.web.model.${table_name};
@Repository 
public class ${table_name}DaoImpl extends BaseDao implements ${table_name}Dao{
    
	public PageEntity query${table_name}List(PageEntity pageEntity)  throws BusinessException{
		String sql="select * from ${table_name_small}";
		pageEntity.setSql(sql);
		return  this.queryForPageEntity(pageEntity);
	}
	public ${table_name} query${table_name}ByID(String id)  throws BusinessException{
		return  this.get(${table_name}.class, id);
	}
	public ${table_name} add${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		//校验是否为空
		if(${table_name?uncap_first}==null){
			throw new BusinessException("传入数据为空！");
		}
		if(StringUtil.isEmpty(${table_name?uncap_first}.getId())){
			this.save(${table_name?uncap_first});
		}else{
			throw new BusinessException("已存在数据！");
		}
		return ${table_name?uncap_first};
	}
	public ${table_name} update${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		${table_name} ${table_name?uncap_first}1= this.get(${table_name}.class, ${table_name?uncap_first}.getId());
		if(${table_name?uncap_first}1==null||StringUtil.isEmpty(${table_name?uncap_first}1.getId())){
	 		throw new BusinessException("没有找到数据！");
		}else{
			BeanUtils.copyProperties(${table_name?uncap_first}1, ${table_name?uncap_first});
			this.update(${table_name?uncap_first}1);
		}
		return ${table_name?uncap_first}1;
	}
	public Boolean delete${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		this.delete(this.get(${table_name}.class, ${table_name?uncap_first}.getId()));
		
		return true;
	}
}
