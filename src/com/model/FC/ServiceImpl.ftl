package com.ziyue.service;

import java.util.Map;
import com.ziyue.utils.PageModel;
import com.ziyue.bean.model.${table_name};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ziyue.dao.${table_name}DAO;



/**
* @ClassName: ${table_name}Service
* @Description:${table_annotation}
* @author ${author}
* @date ${date}
*/
@Service
public class ${table_name}Service{
	@Autowired
	private  ${table_name}DAO  ${table_name?uncap_first}dao;
	/***
	 * 添加学校校区信息表
	 */
	public void add${table_name}(${table_name} ${table_name?uncap_first}) {
		 ${table_name?uncap_first}dao.addData(${table_name?uncap_first});
	}

	/**
	 * 修改学校校区信息表
	 */
	public void modify${table_name}(${table_name} ${table_name?uncap_first}) {
		${table_name?uncap_first}dao.modifyData(${table_name?uncap_first});
	}
	/***
	 * 删除学校校区信息表
	 */
	public void del${table_name}(${table_name} ${table_name?uncap_first}) {
		${table_name?uncap_first}dao.deleteData(${table_name?uncap_first});
	}
	/***
	 * 根据Id查找学校校区信息表
	 */
	public ${table_name} find${table_name}ById(String id) {
		return ${table_name?uncap_first}dao.find${table_name}ById(id);
	}
	
	/**
	 * 分页条件查询
	 * @param  PageModel pageModel 分页封装类
	 * @return PageModel 分页封装类
	 */
	public PageModel findBaseSplitPage(PageModel pageModel) {
		return ${table_name?uncap_first}dao.findBaseSplitPage(pageModel);
	}
	
	/**
	 * 导出excel
	 * @return 记录数
	 */
	public int exp${table_name}(Map<String, String> cols,String expFile,String FIELD,String sheetname){
		return ${table_name?uncap_first}dao.exp${table_name}(cols, expFile,FIELD,sheetname);
	}


}



