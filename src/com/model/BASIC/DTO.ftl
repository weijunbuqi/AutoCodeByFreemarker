package com.fps.web.dto;
import java.util.Date;

/**
* @ClassName: ${table_name}DTO
* @Description:${table_annotation}
* @author ${author}
* @date ${date}
*/


public class ${table_name}DTO{
   
    private String id;
    <#if model_column?exists>
        <#list model_column as model>
    /*${model.columnComment!}*/
    <#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
    private String ${model.changeColumnName?uncap_first};

    </#if>
    <#if (model.columnType = 'INT')>
    private int ${model.changeColumnName?uncap_first};

    </#if>
    <#if model.columnType = 'DATETIME' >
    private Date ${model.changeColumnName?uncap_first};

    </#if>
        </#list>
    </#if>
    public String getId() {
    	return id;
    }
    public void setId(String id) {
    	this.id = id;
    }
<#if model_column?exists>
<#list model_column as model>
<#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
    public String get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(String ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
<#if (model.columnType = 'INT')>
    public int get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(int ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
<#if model.columnType = 'DATETIME' >
    public Date get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(Date ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
</#list>
</#if>

}


