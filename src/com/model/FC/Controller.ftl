package com.ziyue.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import org.apache.shiro.authz.annotation.RequiresPermissions;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.ziyue.service.${table_name}Service;
import com.ziyue.utils.FileLoad;
import com.ziyue.utils.HttpResult;
import com.ziyue.utils.PageModel;
import com.ziyue.utils.StringUtil;
import com.ziyue.utils.Token;
import com.ziyue.entity.BaseUser;
import com.ziyue.bean.model.${table_name};


/**
 * ${table_annotation}页面
 *
 */
@Controller
@RequestMapping("/${table_name?lower_case}")
public class ${table_name}Controller{
    
    @Autowired
    private ${table_name}Service ${table_name?uncap_first}Service;
    
    
 
   
    /**
	 * 加载List数据
	 */
	@RequestMapping("/list")
	public ModelAndView list(HttpServletRequest request) {
		 PageModel pagemodel = PageModel.pageModel(request);
		 pagemodel = ${table_name?uncap_first}Service.findBaseSplitPage(pagemodel);
		 ModelAndView mav = new ModelAndView();
		 mav.addObject("pageModel", pagemodel);
		 mav.setViewName("${table_name?uncap_first}/list");
		 return mav;
	}
	
	
	 
    /**
	 * 跳转add界面
	 */
  	@RequestMapping("/turnAdd")
	public ModelAndView turnAdd${table_name}() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("${table_name?uncap_first}/add");
		return mav;
	}
    /**
	 * 跳转修改界面
	 */
    @RequestMapping("/turnModify")
    @RequiresPermissions("${table_name?uncap_first}:list")
	public ModelAndView turnModify${table_name}(String id) {
		${table_name} ${table_name?uncap_first} =${table_name?uncap_first}Service.find${table_name}ById(id);
		ModelAndView mav = new ModelAndView();
		mav.addObject("${table_name?uncap_first}", ${table_name?uncap_first});
		mav.setViewName("${table_name?uncap_first}/modify");
		return mav;
	}
    
     /**
	 * 新增
	 */
  	@RequestMapping("/add")
    @RequiresPermissions("${table_name?uncap_first}:list")
	public  @ResponseBody HttpResult add(HttpServletRequest request,HttpServletResponse response,${table_name} ${table_name?uncap_first}) {
		if (!Token.validToken(request)) {
			return HttpResult.error("请不要重复操作");
		}
		BaseUser user = (BaseUser)request.getSession().getAttribute("user");
		${table_name?uncap_first}.setCreateid(user.getId());
		${table_name?uncap_first}.setCreater(user.getRealname());
		${table_name?uncap_first}.setCreatetime(new Date());
		${table_name?uncap_first}.setUuid(StringUtil.getUUID());
		try{
			${table_name?uncap_first}Service.add${table_name}(${table_name?uncap_first});
			return HttpResult.success();	
		}catch(Exception e){
			e.printStackTrace();
			return HttpResult.error(e.getMessage());
		}
	}	
  	/**
	 * 修改
	 */
	@RequestMapping("/modify")
    @RequiresPermissions("${table_name?uncap_first}:list")
	public  @ResponseBody HttpResult modify${table_name}(HttpServletRequest request,HttpServletResponse response,${table_name} ${table_name?uncap_first}) {
		if (!Token.validToken(request)) {
			return HttpResult.error("请不要重复操作");
		}
		try{
			BaseUser user = (BaseUser)request.getSession().getAttribute("user");
			${table_name?uncap_first}.setEditid(user.getId());
			${table_name?uncap_first}.setEditer(user.getRealname());
			${table_name?uncap_first}.setEdittime(new Date());
			${table_name?uncap_first}Service.modify${table_name}(${table_name?uncap_first});
			return HttpResult.success();	
		}catch(Exception e){
			e.printStackTrace();
			return HttpResult.error(e.getMessage());
		}
	}	

  	/**
	 * 删除
	 */
	@RequestMapping("/delete")
    @RequiresPermissions("${table_name?uncap_first}:list")
	public @ResponseBody HttpResult  delete(String id) {
		try{
		
			${table_name} ${table_name?uncap_first} = new ${table_name}();
			${table_name?uncap_first}.setUuid(id);
			${table_name?uncap_first}Service.del${table_name}(${table_name?uncap_first});
			return HttpResult.success();	
		}catch(Exception e){
			e.printStackTrace();
			return HttpResult.error(e.getMessage());
		}
	}	
   
   	/**
	 * 详情
	 */
	@RequestMapping("/detail")
	public ModelAndView detail(String id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("${table_name?uncap_first}",${table_name?uncap_first}Service.find${table_name}ById(id));
		mav.setViewName("${table_name?uncap_first}/detail");
		return mav;
	}
	
	
	
	@RequestMapping("/excelExp")
	public void excelExp(HttpServletRequest request,HttpServletResponse response,String format,String sheetname,String FIELD) throws Exception{
		//判断后缀是否有效
		if(null == format || "csv,xlsx".indexOf(format.toLowerCase()) == -1  ){
			format = "xlsx";
		}
		//指定要导出的文件,导出文件放到/attached/temp文件夹下，系统会自动清除
		String expFile =
				 request.getSession().getServletContext().getRealPath("/") 
				 + "/attached/temp/" +  StringUtil.getUUID()+"."+format;
		Map<String, String> cols = new HashMap<String, String>();
		int total = ${table_name?uncap_first}Service.exp${table_name}(cols, expFile,FIELD,sheetname);
		if(total < 1){
			FileLoad.downFail(response, null,total);
		}else{
			FileLoad.download(expFile,sheetname+"."+format,request,response);	
		}
	}		
}
