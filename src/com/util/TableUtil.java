package com.util;

public class TableUtil {
    //表注释
    private  String tableAnnotation;

    //模板位置以及目录
    private  String modelname;

    //表名
    private  String tablename;

    //生成路径
    private  String fillpath;


    //后缀名
    private  String suffixname;


    public String getTableAnnotation() {
        return tableAnnotation;
    }

    public void setTableAnnotation(String tableAnnotation) {
        this.tableAnnotation = tableAnnotation;
    }

    public String getModelname() {
        return modelname;
    }

    public void setModelname(String modelname) {
        this.modelname = modelname;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getFillpath() {
        return fillpath;
    }

    public void setFillpath(String fillpath) {
        this.fillpath = fillpath;
    }

    public String getSuffixname() {
        return suffixname;
    }

    public void setSuffixname(String suffixname) {
        this.suffixname = suffixname;
    }
}
