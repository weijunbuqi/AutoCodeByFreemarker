package com.util;

import java.util.List;

public class ProjectUtil {

    //本地目录
    private   String DISKPATH;

    //项目名称
    private  String projectname;

    //文件件目录list
    private List<String> FileDirectory;

    //表结构list
    private List<TableUtil> tables;





    public List<TableUtil> getTables() {
        return tables;
    }

    public void setTables(List<TableUtil> tables) {
        this.tables = tables;
    }

    public String getDISKPATH() {
        return DISKPATH;
    }

    public void setDISKPATH(String DISKPATH) {
        this.DISKPATH = DISKPATH;
    }

    public List<String> getFileDirectory() {
        return FileDirectory;
    }

    public void setFileDirectory(List<String> fileDirectory) {
        FileDirectory = fileDirectory;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }
}
