<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link href="${r"${"}pageContext.request.contextPath}/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<title>${r"${"}applicationScope.SYS_NAME}</title>
<base target="_self" />
<link href="${r"${"}pageContext.request.contextPath}/css/index.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="${r"${"}pageContext.request.contextPath}/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="${r"${"}pageContext.request.contextPath}/js/dialog/layer/layer.js"></script>
<script type="text/javascript" src="${r"${"}pageContext.request.contextPath}/AdminLTE/plugins/icheck/icheck.js"></script>
<style>
	.tbj{
		width: 120px;
	}
</style>
<script type="text/javascript">
function saveOption(){
		submitForm();
} 	


var submitFlag = true ;
function submitForm(){
	 //提交表单
	 $.post(document.creatorForm.action, $("#creatorForm").serialize(), function(data) {
		 if(data.status == "ok"){
			 parent.location.reload();
		 }else{
			 layer.alert(data.msg , {title:'操作失败',icon: 2,shade: 0.05 });
		 }
 	 }); 
}

</script>
</head>
<body>
<div class="right_add">
	<div class="title"><span>请填写${table_annotation}</span></div>
	<form action="${r"${"}pageContext.request.contextPath}/${table_name?lower_case}/add.do"  id="creatorForm" name="creatorForm" method="post"  autocomplete="off"  >
		<input type="hidden" name="SesToken" value="${r"${"}SesToken }"/>
	   	<div style="margin:0 auto; text-align:left; width:100%; ">
			<table class="add_tab" width="100%"  cellpadding="0" cellspacing="0">
		      
		       <#if model_column?exists>
				<#list model_column as model>
				<tr>
		         	<td  class="tbj">${model.columnComment!}</td>
		         	<td><input id="${model.changeColumnName?lower_case}"  name="${model.changeColumnName?lower_case}" class="input_text" type="text"  maxlength="32" /></td>
		      	</tr>
				</#list>
				</#if>
		   </table>
		   		<div class="bn">
		  		 	<input  value="关 闭" type="button" class="but3d white bigrounded"  id="btn-close"/>
	    		  	<input class="but3d blue bigrounded"  value="添 加" type="button" onclick="javascript:saveOption();"/>
	    		</div>
	    	</div>
	    </form>
	</div>
</body>
</html>