package com.fps.web.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
* @ClassName: ${table_name}
* @Description:${table_annotation}
* @author ${author}
* @date ${date}
*/

@Entity
@Table(name="${table_name_small?upper_case}")
public class ${table_name} {
    @Id
    @GenericGenerator(name="systemUUID",strategy="uuid")  
    @GeneratedValue(generator="systemUUID")  
    @Column(name = "id", insertable = true, updatable = true, nullable = false)
    private String id;
    <#if model_column?exists>
        <#list model_column as model>
    /*${model.columnComment!}*/
    <#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
    private String ${model.changeColumnName?uncap_first};

    </#if>
    <#if (model.columnType = 'INT')>
    private int ${model.changeColumnName?uncap_first};

    </#if>
    <#if model.columnType = 'DATETIME' >
    private Date ${model.changeColumnName?uncap_first};

    </#if>
        </#list>
    </#if>
    public String getId() {
    	return id;
    }
    public void setId(String id) {
    	this.id = id;
    }
<#if model_column?exists>
<#list model_column as model>
<#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
    public String get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(String ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
<#if (model.columnType = 'INT')>
    public int get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(int ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
<#if model.columnType = 'DATETIME' >
    public Date get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(Date ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
</#list>
</#if>

}


