package com.creat;

import java.sql.*;

import com.util.Constant;
//TODO 暂时放弃
public class LinkSQL {
    //我么要执行创建表的DDl语句
    static String creatsql = "CREATE TABLE LANDINFO\r\n" + 
    		"(\r\n" + 
    		"   UUID                 VARCHAR(32) NOT NULL COMMENT '流水号',\r\n" + 
    		"   CREATER              VARCHAR(32) COMMENT '创建人',\r\n" + 
    		"   CREATETIME           DATETIME COMMENT '创建时间',\r\n" + 
    		"   CREATEID             VARCHAR(32) COMMENT '创建人id',\r\n" + 
    		"   EDITER               VARCHAR(32) COMMENT '修改人',\r\n" + 
    		"   EDITTIME             DATETIME COMMENT '修改时间',\r\n" + 
    		"   EDITID               VARCHAR(32) COMMENT '修改人id',\r\n" + 
    		"   ORDERBY              VARCHAR(32) COMMENT '排序',\r\n" + 
    		"   SCHOOLUUID           VARCHAR(32) COMMENT '校区流水号',\r\n" + 
    		"   LANDNO               VARCHAR(32) COMMENT '校内编号',\r\n" + 
    		"   LANDNAME             VARCHAR(32) COMMENT '名称（暂不使用）',\r\n" + 
    		"   POSITION             VARCHAR(32) COMMENT '校内坐落',\r\n" + 
    		"   CERTIFICATETYPE      VARCHAR(32) COMMENT '证书类型',\r\n" + 
    		"   AREALAND             VARCHAR(32) COMMENT '土地面积（平方米）',\r\n" + 
    		"   LANDLV               VARCHAR(32) COMMENT '土地等级',\r\n" + 
    		"   CLOUDNO              VARCHAR(32) COMMENT '资产云编号',\r\n" + 
    		"   POSITIONS            VARCHAR(32) COMMENT '坐落位置',\r\n" + 
    		"   IMGNO                VARCHAR(32) COMMENT '图号',\r\n" + 
    		"   LANDNUM              VARCHAR(32) COMMENT '地号',\r\n" + 
    		"   MMOVABLESNO          VARCHAR(32) COMMENT '不动产单元号',\r\n" + 
    		"   POWERTYPE            VARCHAR(32) COMMENT '权利类型',\r\n" + 
    		"   POWERNATURE          VARCHAR(32) COMMENT '权利性质',\r\n" + 
    		"   ENDTIME              VARCHAR(32) COMMENT '使用期限',\r\n" + 
    		"   LANDAREA             VARCHAR(32) COMMENT '土地证载明面积',\r\n" + 
    		"   DISPOSALMETHOD       VARCHAR(32) COMMENT '处置方式',\r\n" + 
    		"   PURPOSE              VARCHAR(32) COMMENT '用途分类',\r\n" + 
    		"   USEORG               VARCHAR(32) COMMENT '使用部门',\r\n" + 
    		"   MANAGEORG            VARCHAR(32) COMMENT '管理部门',\r\n" + 
    		"   USESTATUS            VARCHAR(32) COMMENT '使用情况',\r\n" + 
    		"   DATEISSUE            VARCHAR(32) COMMENT '发证时间',\r\n" + 
    		"   ASSETNO              VARCHAR(32) COMMENT '清查系统资产编号',\r\n" + 
    		"   FILELOCATION         VARCHAR(32) COMMENT '档案存放位置',\r\n" + 
    		"   PURCHASEORG          VARCHAR(32) COMMENT '采购组织形式',\r\n" + 
    		"   OWNERSHIPNATURE      VARCHAR(32) COMMENT '权属性质',\r\n" + 
    		"   OWNERSHIPCODE        VARCHAR(32) COMMENT '权属证号',\r\n" + 
    		"   ASSETCODE            VARCHAR(32) COMMENT '资产分类代码',\r\n" + 
    		"   FUNDING              VARCHAR(32) COMMENT '财政拨款',\r\n" + 
    		"   VALUETYPE            VARCHAR(32) COMMENT '价值类型',\r\n" + 
    		"   ORGNVALUE            VARCHAR(32) COMMENT '事业收入',\r\n" + 
    		"   WINDFALL             VARCHAR(32) COMMENT '预算外收入',\r\n" + 
    		"   ACCOUNTNO            VARCHAR(32) COMMENT '会计凭证号',\r\n" + 
    		"   FINANCE              VARCHAR(32) COMMENT '财政性结余资金',\r\n" + 
    		"   LENDORG              VARCHAR(32) COMMENT '出借单位',\r\n" + 
    		"   SELFAREA             VARCHAR(32) COMMENT '自用面积',\r\n" + 
    		"   SELFVALUE            VARCHAR(32) COMMENT '自用价值',\r\n" + 
    		"   LENDAREA             VARCHAR(32) COMMENT '出借面积',\r\n" + 
    		"   LENDVALUE            VARCHAR(32) COMMENT '出借价值',\r\n" + 
    		"   FOREIGNAREA          VARCHAR(32) COMMENT '对外投资面积',\r\n" + 
    		"   FOREIGNVALUE         VARCHAR(32) COMMENT '对外投资价值',\r\n" + 
    		"   GUARANTEEAREA        VARCHAR(32) COMMENT '担保面积',\r\n" + 
    		"   GUARANTEEVALUE       VARCHAR(32) COMMENT '担保价值',\r\n" + 
    		"   OTHERAREA            VARCHAR(32) COMMENT '其他面积',\r\n" + 
    		"   OTHERVALUE           VARCHAR(32) COMMENT '其他价值',\r\n" + 
    		"   GETTIME              DATETIME COMMENT '取得时间',\r\n" + 
    		"   GETTYPE              VARCHAR(32) COMMENT '取得方式',\r\n" + 
    		"   BILLTIME             DATETIME COMMENT '单据日期',\r\n" + 
    		"   SINGLEPERSON         VARCHAR(32) COMMENT '制单人',\r\n" + 
    		"   LANDAREAS            VARCHAR(32) COMMENT '土地面积（亩）',\r\n" + 
    		"   TD_HEADERNO          VARCHAR(32) COMMENT '抬头编号',\r\n" + 
    		"   TD_BOTTOMNO          VARCHAR(32) COMMENT '底部编号',\r\n" + 
    		"   TD_LANDUSER          VARCHAR(32) COMMENT '土地使用者',\r\n" + 
    		"   TD_LANDNUM           VARCHAR(32) COMMENT '地号',\r\n" + 
    		"   TD_POSITIONS         VARCHAR(32) COMMENT '座落',\r\n" + 
    		"   TD_IMGNO             VARCHAR(32) COMMENT '图号',\r\n" + 
    		"   TD_PURPOSE           VARCHAR(32) COMMENT '用途',\r\n" + 
    		"   TD_LANDLV            VARCHAR(32) COMMENT '土地等级',\r\n" + 
    		"   TD_USETYPE           VARCHAR(32) COMMENT '使用权类型',\r\n" + 
    		"   TD_ENDTIME           VARCHAR(32) COMMENT '终止日期',\r\n" + 
    		"   TD_USEAREA           VARCHAR(32) COMMENT '使用权面积',\r\n" + 
    		"   TD_TOWNORGAN         VARCHAR(32) COMMENT '城镇机关',\r\n" + 
    		"   TD_STAMPEDDATE       VARCHAR(32) COMMENT '盖章日期',\r\n" + 
    		"   TD_SHARE             VARCHAR(32) COMMENT '其中共用分摊面积',\r\n" + 
    		"   TD_REMARKS           VARCHAR(1024) COMMENT '备注',\r\n" + 
    		"   BDC_PROVINCECODE     VARCHAR(32) COMMENT '省编号',\r\n" + 
    		"   BDC_NO               VARCHAR(32) COMMENT '编号',\r\n" + 
    		"   BDC_POWERUSER        VARCHAR(32) COMMENT '权力人',\r\n" + 
    		"   BDC_CONDITION        VARCHAR(32) COMMENT '共有情况',\r\n" + 
    		"   BDC_POSTION          VARCHAR(32) COMMENT '坐落',\r\n" + 
    		"   BDC_ESTATE           VARCHAR(32) COMMENT '不动产单元号',\r\n" + 
    		"   BDC_POWERTYPE        VARCHAR(32) COMMENT '权力类型',\r\n" + 
    		"   BDC_POWERNATURE      VARCHAR(32) COMMENT '权力性质',\r\n" + 
    		"   BDC_PURPOSE          VARCHAR(32) COMMENT '用途',\r\n" + 
    		"   BDC_AREA             VARCHAR(32) COMMENT '面积',\r\n" + 
    		"   BDC_USEPOWER         VARCHAR(32) COMMENT '使用期限',\r\n" + 
    		"   BDC_POWERPURPOSE     VARCHAR(32) COMMENT '权力其他状况',\r\n" + 
    		"   BDC_REMARKS          VARCHAR(32) COMMENT '备注',\r\n" + 
    		"   BDC_ORDERBY          VARCHAR(32) COMMENT '序号',\r\n" + 
    		"   BDC_LOCATION         VARCHAR(32) COMMENT '所在层',\r\n" + 
    		"   BDC_FLOOR            VARCHAR(32) COMMENT '总层数',\r\n" + 
    		"   BDC_PLANNING         VARCHAR(32) COMMENT '规划用途',\r\n" + 
    		"   BDC_BUILDAREA        VARCHAR(32) COMMENT '建筑面积',\r\n" + 
    		"   BDC_PROPRIETARY      VARCHAR(32) COMMENT '专有面积',\r\n" + 
    		"   BDC_APPORTIONMENT    VARCHAR(32) COMMENT '分摊面积',\r\n" + 
    		"   BDC_COMPLETION       VARCHAR(32) COMMENT '竣工年份',\r\n" + 
    		"   BDC_HOUSINGSTRUCTURE VARCHAR(32) COMMENT '房屋结构',\r\n" + 
    		"   ISDELETE             VARCHAR(2) COMMENT '是否删除',\r\n" + 
    		"   PRIMARY KEY (UUID)\r\n" + 
    		");\r\n" + 
    		"";
    public static void main(String[] args){
        Connection conn = null;
        Statement stmt = null;
        try{
            //注册jdbc驱动
            Class.forName(Constant.MYSQLDBDRIVER);
            //打开连接
            System.out.println("//连接数据库");
            conn = DriverManager.getConnection(Constant.MYSQLDBURL,Constant.MYSQLDBUSER,Constant.MYSQLDBPASSWORD);
            //执行创建表
            System.out.println("//创建表");
            stmt = conn.createStatement();
            if(0 == stmt.executeLargeUpdate(creatsql)) {
                System.out.println("成功创建表！");
            }
            else {
                System.out.println("创建表失败！");
            }
            stmt.close();
            conn.close();
            System.out.println("//关闭资源");
        }
        catch(Exception e) {
            System.out.println("创建表失败！");
            e.printStackTrace();
        }

    }
}