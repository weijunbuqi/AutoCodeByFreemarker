package com.mengbao.model.bean;
import java.util.Date;


/**
* @ClassName: ${table_name}
* @Description:${table_annotation}
* @author ${author}
* @date ${date}
*/


public class ${table_name} {
  

    <#if model_column?exists>
        <#list model_column as model>
    /*${model.columnComment!}*/
    <#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
    private String ${model.changeColumnName?uncap_first};

    </#if>
    <#if (model.columnType = 'INT')>
    private int ${model.changeColumnName?uncap_first};

    </#if>
    <#if model.columnType = 'DATETIME' >
    private Date ${model.changeColumnName?uncap_first};

    </#if>
        </#list>
    </#if>
  
<#if model_column?exists>
<#list model_column as model>
<#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
    public String get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(String ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
<#if (model.columnType = 'INT')>
    public int get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(int ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
<#if model.columnType = 'DATETIME' >
    public Date get${model.changeColumnName?cap_first}() {
        return this.${model.changeColumnName?uncap_first};
    }

    public void set${model.changeColumnName?cap_first}(Date ${model.changeColumnName?uncap_first}) {
        this.${model.changeColumnName?uncap_first} = ${model.changeColumnName?uncap_first};
    }

</#if>
</#list>
</#if>

}


