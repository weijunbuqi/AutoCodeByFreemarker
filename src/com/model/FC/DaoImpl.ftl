package com.ziyue.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.ziyue.utils.BaseDao;
import com.ziyue.utils.CustomRowMapper;
import com.ziyue.utils.DynamicBeanUtil;
import com.ziyue.utils.ExcelFiledUtil;
import com.ziyue.utils.PageModel;
import com.ziyue.bean.model.${table_name};


/**
* @ClassName: ${table_name}DAO
* @Description:${table_annotation}
* @author ${author}
* @date ${date}
*/ 
@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ${table_name}DAO extends BaseDao{
    
	/***
	 * 根据Id查找学校校区信息表
	 */
	public ${table_name} find${table_name}ById(String id){
		try{
			String sql="select * from ${table_name_small} where  uuid=? ";
			return (${table_name})jdbcTemplate.queryForObject(sql,new Object[]{id}, new BeanPropertyRowMapper(${table_name}.class));
		}catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 分页条件查询
	 * @param  PageModel pageModel 分页封装类
	 * @return PageModel 分页封装类
	 */
	public PageModel findBaseSplitPage(PageModel pageModel){
		String sqlCount="select count(*) from ${table_name_small} n  where 1=1 ";
		String sqlQuery="select *  from ${table_name_small} n  where 1=1 ";

	    Map<String,String> search=pageModel.getSearch();//搜索条件
	    List parmlist=pageModel.getParmlist();          //查询参数
		
		StringBuffer sql=new StringBuffer("");
		if( null!=search ){
			for(String key : search.keySet()){
	            if("title".equals(key)){
	            	sql.append(" and n.title like ? ");
	            	parmlist.add("%"+search.get(key).trim()+"%");
	            }
		    }
		}
		sqlCount=sqlCount+sql;	
		sqlQuery=sqlQuery+sql+" order by n.createtime  desc ";
		//设置分页统计总记录数sql
		pageModel.setSqlCount(sqlCount);
		//设置分页统计要查询结果集sql
		pageModel.setSqlQuery(sqlQuery);
		return super.queryForPageModel(pageModel);
	}
	
	/**
	 * 导出excel
	 * @return 记录数
	 */
	public int exp${table_name}(Map<String, String> cols,String expFile,String FIELD,String sheetname){
		StringBuffer sql=new StringBuffer("");
		sql.append("select *  from ${table_name_small} n  where 1=1  ");
		if(null != cols){
			for(String key : cols.keySet() ){
				if("landno".equals(key)){
	            	sql.append(" and n.landno like '%"+cols.get(key).trim()+"%' ");
	            }
			}
		}
		List<DynamicBeanUtil>  dynamicBeanUtils= (List<DynamicBeanUtil>)jdbcTemplate.query(sql.toString(),  new ArrayList<String>().toArray(),new CustomRowMapper());
		ExcelFiledUtil.expXlsx(expFile, sheetname,FIELD,dynamicBeanUtils);
		return dynamicBeanUtils.size();
	}
	
	
}
