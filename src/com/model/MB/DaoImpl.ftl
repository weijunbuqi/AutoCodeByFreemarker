package com.mengbao.dao.impl;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import com.mengbao.common.DBsetup;
import com.mengbao.common.JavaReflect;
import com.mengbao.common.ModelDao;
import com.mengbao.model.bean.${table_name};
import com.mengbao.utils.StringUtil;


public class ${table_name}DaoImpl{
    
	private String table;
	private List<Field> allField;
	private String primaryKey;
	private static QueryRunner qr = DBsetup.getRunner();
    
    private static ${table_name}DaoImpl ${table_name_small}DaoImpl;
	public static ${table_name}DaoImpl get${table_name}DaoImpl(){
		if(null==${table_name_small}DaoImpl){
			${table_name_small}DaoImpl = new ${table_name}DaoImpl();
		}
		return ${table_name_small}DaoImpl;
	}
	public ${table_name}DaoImpl() {
		this.table = "`${table_name_small}`";
		this.allField = JavaReflect.getFields("com.mengbao.model.bean.${table_name}");
		this.primaryKey = "uuid";
	}
    
	
	public boolean insert(${table_name} ${table_name_small}) throws SQLException{
		return ModelDao.insert(${table_name_small}, table, allField);
	}
	public boolean replace(${table_name} obj) throws SQLException{
		return ModelDao.replace(obj, table, allField);
	}
	
	public boolean batch(List<${table_name}> list) throws SQLException{
		boolean result = ModelDao.batch(list, table, allField);
		return result;
	}
	
	public boolean batchReplace(List<${table_name}> list) throws SQLException{
		boolean result = ModelDao.batchReplace(list, table, allField);
		return result;
	}
	
	public boolean deleteByPrimaryKey(String obj) throws SQLException {
		return ModelDao.deleteByPrimaryKey(obj, table, primaryKey);
	}
	
	public boolean batchDeleteByPrimaryKey(List<String> UUIDList) throws SQLException{
		return ModelDao.batchDeleteByPrimaryKey(UUIDList, table, primaryKey);
	}
	
	public boolean updateByPrimaryKey(${table_name} obj) throws SQLException {
		return ModelDao.updateByPrimaryKey(obj, table, allField, primaryKey);
	}

	public boolean updateByPrimaryKey(String attndUUID) throws SQLException {
		return ModelDao.updateByPrimaryKey(attndUUID, table, allField, primaryKey);
	}
	
	public ${table_name} selectSingleByPrimaryKey(String attndUUID) throws SQLException{
		return ModelDao.selectSingleByPrimaryKey(attndUUID, table, allField, primaryKey, ${table_name}.class);
	}
	
	public List<${table_name}> selectList(${table_name} obj, String order) throws SQLException{
		return ModelDao.selectList(obj, table, allField,${table_name}.class, order, null);
	}
	
	public List<${table_name}> selectAll(String otherExample, String order) throws SQLException{
		return ModelDao.selectAll(table, allField, ${table_name}.class, otherExample, order, null);
	}
	
	
	
}
