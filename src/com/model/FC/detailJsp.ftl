<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link href="${r"${"}pageContext.request.contextPath}/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<title>${r"${"}applicationScope.const.SYS_NAME}</title>
<base target="_self" />
<link href="${r"${"}pageContext.request.contextPath}/css/index.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="${r"${"}pageContext.request.contextPath}/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="${r"${"}pageContext.request.contextPath}/js/dialog/lhgdialog.min.js?skin=discuz"></script>
<script type="text/javascript" src="${r"${"}pageContext.request.contextPath}/js/dialog/lhg-custom.js"></script>
<style>
	.tbj{
		width: 120px;
	}
</style>
</head>
<body class="detail-bg">
	<div class="detail-tittle">
		<h1>${table_annotation}详情 <span onclick="javascript:window.close();"></span></h1>
	</div>
	<div class="detail-wrap">
			 <#if model_column?exists>
				<#list model_column as model>
				 <#if model_index==0>
				 	<div class="clearfix"> 
				 </#if>
						<div class="item r4"><div class="label">${model.columnComment!}</div>
       					<div class="txt">${r"${"}${table_name?lower_case}.${model.changeColumnName?lower_case} }</div></div>
				 <#if (model_index+1)%4==0&&model_index!=0&&model_has_next>
				 	</div>
				 	<div class="clearfix"> 
				 </#if>
				  <#if !model_has_next>
				 	</div>
				 </#if>
				</#list>
			</#if>
	</div>
</body>
</html>
