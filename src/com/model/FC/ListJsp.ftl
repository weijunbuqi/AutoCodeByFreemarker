<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link href="${r"${"}pageContext.request.contextPath${r"}"}/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<title>${r"${"}applicationScope.SYS_NAME${r"}"}</title>
<link href="${r"${"}pageContext.request.contextPath${r"}"}/css/index.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="${r"${"}pageContext.request.contextPath${r"}"}/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="${r"${"}pageContext.request.contextPath${r"}"}/js/dialog/layer/layer.js"></script>
<script type="text/javascript" src="${r"${"}pageContext.request.contextPath${r"}"}/AdminLTE/plugins/icheck/icheck.js"></script>
<script type="text/javascript">
function turnAdd(){
	var url = "${r"${"}pageContext.request.contextPath${r"}"}/${table_name?lower_case}/turnAdd.do";
	var title = "${table_annotation}";
	showDialog(title,url,1000,500);
}

function turnModify(id){
	var url = "${r"${"}pageContext.request.contextPath${r"}"}/${table_name?lower_case}/turnModify.do?id="+id;
	var title = "${table_annotation}";
	showDialog(title,url,1000,500);
}

function deleteOption(id){
	var url = "${r"${"}pageContext.request.contextPath${r"}"}/${table_name?lower_case}/delete.do?id="+id;
	openWhenSplitPage(url);

}


function detailOption(id){
	window.open("${r"${"}pageContext.request.contextPath${r"}"}/${table_name?lower_case}/detail.do?id="+id);  
}
</script>
</head>

<body>
<div class="right_nav"> 
	<h1 class="guide">当前操作:查询${table_annotation}</h1>
</div>
<div class="right_list">	
	<div class="sou">
		<form action="${r"${"}pageContext.request.contextPath${r"}"}/${table_name?lower_case}/list.do" method="post"  autocomplete="off"  >
		<div class="divli"><p>标题</p><input  class="sou_input_text" name="title" type="text" value="${r"${"}pageModel.search.title${r"}"}"/></div>
		 <div class="divsubmit"> <input type="submit" class="but3d blue medium"  value="搜 索" /></div>
		</form>	
		<div class="clear"></div>
	</div>
    <div class="title">
    	<h1>${table_annotation}</h1>
  		<input class="but3d blue small" type="button" value="新增" onclick="javascript:turnAdd();"/>
    </div>
   	<div class="table-wrap">   
    <table width="100%" cellpadding="0" cellspacing="0" class="right_tab">
    	<tr class="tr_head" >
	      	<td width="50px">序号</td>
	      	 <#if model_column?exists>
				<#list model_column as model>
				<td>${model.columnComment!}</td>
				</#list>
				</#if>
	      	
	        
	        <td  width="80px">操作</td>
		</tr>
     	<c:if test="${r"${"}empty pageModel.data ${r"}"}">
     		<tr class="tr_yellow" >
      			<td colspan="12">没有您要查询的内容</td>
      		</tr>
     	</c:if>
        <c:forEach items="${r"${"}pageModel.data ${r"}"}" var="${table_name?lower_case}" varStatus="status">
	    	 <tr  <c:if test="${r"${"}status.count % 2 == 0 ${r"}"}">class="tr_even"</c:if> >
		     	<td>${r"${"}(pageModel.curpage-1) * pageModel.pagecount+ status.count ${r"}"}</td>
		     	 <#if model_column?exists>
				<#list model_column as model>
				<td>${r"${"}${table_name?lower_case}.${model.changeColumnName?lower_case} }</td>
				</#list>
				</#if>
		        <!--  <td><a href="javascript:detailOption('${r"${"}${table_name?lower_case}.uuid ${r"}"}');">${r"${"}${table_name?lower_case}.uuid ${r"}"}</a></td>
		        -->
		        <td>
			        <a href="javascript:turnModify('${r"${"}${table_name?lower_case}.uuid ${r"}"}');">修改</a>
			        <a href="javascript:deleteOption('${r"${"}${table_name?lower_case}.uuid ${r"}"}');">删除</a>
		        </td>
	       </tr>
        </c:forEach>
		     
 	</table>
 	
 	<form action="${r"${"}pageContext.request.contextPath${r"}"}/${table_name?lower_case}/list.do" method="post"  autocomplete="off"   name="splitpageform">
				<jsp:include page="../public/splitpage.jsp"></jsp:include> 
	</form>
 	</div>
</div>

</body>
</html>

