package com.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class ConstantUtil {

    //数据库链接地址
    private  String MYSQLDBURL = "jdbc:mysql://localhost:3306/autocode?characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC";
    private  String MYSQLDBDRIVER = "com.mysql.cj.jdbc.Driver";
    private  String MYSQLDBUSER = "root";
    private  String MYSQLDBPASSWORD = "123456";
    private  String AUTHOR = "Spf";
    private  String CURRENT_DATE = new SimpleDateFormat("yyyy-MM-dd").format((Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()));
    private  String packageName = "com";


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getCURRENT_DATE() {
        return CURRENT_DATE;
    }

    public void setCURRENT_DATE(String CURRENT_DATE) {
        this.CURRENT_DATE = CURRENT_DATE;
    }

    //生成路径
    private  String DISKPATH = "G:\\30\\AutoCode\\table/";


    public String getMYSQLDBURL() {
        return MYSQLDBURL;
    }

    public void setMYSQLDBURL(String MYSQLDBURL) {
        this.MYSQLDBURL = MYSQLDBURL;
    }

    public String getMYSQLDBDRIVER() {
        return MYSQLDBDRIVER;
    }

    public void setMYSQLDBDRIVER(String MYSQLDBDRIVER) {
        this.MYSQLDBDRIVER = MYSQLDBDRIVER;
    }

    public String getMYSQLDBUSER() {
        return MYSQLDBUSER;
    }

    public void setMYSQLDBUSER(String MYSQLDBUSER) {
        this.MYSQLDBUSER = MYSQLDBUSER;
    }

    public String getMYSQLDBPASSWORD() {
        return MYSQLDBPASSWORD;
    }

    public void setMYSQLDBPASSWORD(String MYSQLDBPASSWORD) {
        this.MYSQLDBPASSWORD = MYSQLDBPASSWORD;
    }

    public String getDISKPATH() {
        return DISKPATH;
    }

    public void setDISKPATH(String DISKPATH) {
        this.DISKPATH = DISKPATH;
    }
}
